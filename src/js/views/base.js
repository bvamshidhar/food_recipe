export const elements = {
    searchForm: document.querySelector('.search'),
    searchInput: document.querySelector('.search__field'),
    searchRes: document.querySelector('.results'),
    searchResList: document.querySelector('.results__list'),
    searchResPages: document.querySelector('.results__pages'),
    recipe: document.querySelector('.recipe'),
    shopping: document.querySelector('.shopping__list'),
    likesMenu: document.querySelector('.likes__field'),
    likesList: document.querySelector('.likes__list')
};

export const elementStrings = {
    loader: 'loader'
};

export const renderLoader = parent => {
    const loader = `
        <div class="${elementStrings.loader}">
            <svg>
                <use href="img/icons.svg#icon-cw"></use>
            </svg>
        </div>
    `;
    parent.insertAdjacentHTML('afterbegin', loader);
};

// here we cannot select the loader like we did in the elements because by the time that this code runs,
// the loader is not yet on the page. Therefore we create the elementStrings and access the loader form that object to maintain DRY.
export const clearLoader = () => {
    const loader = document.querySelector(`.${elementStrings.loader}`);
    // to delete an element form the DOM, we have to go up the parent element, then we move down and remove the child element
    if(loader) {
        loader.parentElement.removeChild(loader);
    }
};