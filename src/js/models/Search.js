import axios from 'axios';


export default class Search {
    constructor(query) {
        this.query = query;
    }

    async getResults() {
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        try {
            const res = await axios(`${proxy}https://forkify-api.herokuapp.com/api/search?&q=${this.query}`);
            this.result = res.data.recipes;
            // console.log(this.result);
        }
        catch(error) {
            alert(error);
        }
    }
};


// axios is used to do our api calls. It works exactly like fetch()
// axios works on all the browsers unlike fetch which works only on modern browsers
// It has some more aspects like, it automatically returns json whereas
// in fetch, first we had to wait for the data to come back then we had to wait for it to convert to json
// axios is better at error handling than fetch

