import axios from 'axios';

export default class Recipe {
    constructor(id) {
        this.id = id;
    }

    async getRecipe() {
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        try {
            const res = await axios(`${proxy}https://forkify-api.herokuapp.com/api/get?rId=${this.id}`);
            this.title = res.data.recipe.title;
            this.author = res.data.recipe.publisher;
            this.image = res.data.recipe.image_url;
            this.url = res.data.recipe.source_url;
            this.ingredients = res.data.recipe.ingredients;
        }
        catch(error) {
            console.log(error);
            alert('Something went wrong')
        }
    }

    calcTime() {
        // Assuming that we need 15 minutes for each ingredient
        const numIng = this.ingredients.length;
        const periods = Math.ceil(numIng/3);
        this.time = periods * 15
    }

    calcServings() {
        this.servings = 4;
    }

    parseIngredients() {
        const unitsLong = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds'];
        const unitsShort = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
        const units = [...unitsShort, 'kg', 'g'];

        // We basically loop over ingredients where in each iteration, we can save new item to this newIngredients array
        const newIngredients = this.ingredients.map(el => {
            // 1. Uniform units
            let ingredient = el.toLowerCase();
            // in this callback function we take current element(unit) and currect index(i)
            unitsLong.forEach((unit, i) => {
                ingredient = ingredient.replace(unit, unitsShort[i])
            });

            // 2. Remove parantheses
            ingredient = ingredient.replace(/ *\([^)]*\) */g, ' ');

            // 3. Parse ingredients into count, unit and ingredient
            const arrIng = ingredient.split(' ');
            // We cannot use indexOf method because we don't know what unit we are searching or what unit we have in particular
            // So, we can check if the current element is inside the array
            // So, for each current element, we check if that element is inside of the unitsShort array
            const unitIndex = arrIng.findIndex((el2) => unitsShort.includes(el2));

            let objIng;
            if(unitIndex > -1) {
                // There is a unit
                // Ex: 4 1/2 tsp sugar, arrCount = [4, 1/2]
                // Ex: 4 cups, arrCount = [4]
                // Ex for eval: [4, 1/2] --> eval("4+1/2") = 4.5
                const arrCount = arrIng.slice(0, unitIndex);

                let count = 0;
                if(arrCount.length === 1) {
                    // for data that has 1-1/2 tsp, it evaluates to (1+1/2) tsp
                    count = eval(arrIng[0].replace('-', '+'));
                }
                else {
                    count = eval(arrIng.slice(0, unitIndex).join('+'));
                }
                
                objIng = {
                    count,
                    unit: arrIng[unitIndex],
                    ingredient: arrIng.slice(unitIndex + 1).join(' ')       // here, the ingredient starts right after th unitIndex
                };

            }
            else if(parseInt(arrIng[0], 10)) {
                // There is no unit but the 1st element is a number
                objIng = {
                    count: parseInt(arrIng[0], 10),
                    unit: '',
                    ingredient: arrIng.slice(1).join(' ')       // the ingredient is the complete arrIng except the 1st element
                };

            }
            else if(unitIndex === -1) {
                // There is no unit and no number in the 1st position
                objIng = {
                    count: 1,       // minimum ingredient count
                    unit: '',
                    ingredient      // shorthand for ingredient: ingredient
                };
            }

            return objIng;
        });
        this.ingredients = newIngredients;        
    }

    updateServings(type) {
        // Servings
        const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;

        // Ingredients
        this.ingredients.forEach(ing => {
            ing.count *= (newServings / this.servings)
        });

        this.servings = newServings;
    }
}

